# Tutorial 1 SBF PTI


## What we will learn:
- HTML : https://www.w3schools.com/html/html_intro.asp
- CSS : https://www.w3schools.com/Css/css_intro.asp
- CSS (flex) : https://css-tricks.com/snippets/css/a-guide-to-flexbox/
- CSS (media query) : https://www.w3schools.com/cssref/css3_pr_mediaquery.asp
- Javascript: https://www.w3schools.com/whatis/whatis_js.asp

#### What is HTML?
- HTML stands for Hyper Text Markup Language
- HTML describes the structure of a Web page
- HTML consists of a series of elements
- HTML elements tell the browser how to display the content
- HTML elements are represented by tags
- HTML tags label pieces of content such as "heading", "paragraph", "table", and so on
- Browsers do not display the HTML tags, but use them to render the content of the page

#### What is CSS?
- CSS stands for Cascading Style Sheets
- CSS describes how HTML elements are to be displayed on screen, paper, or in other media
- CSS saves a lot of work. It can control the layout of multiple web pages all at once
- External stylesheets are stored in CSS files

#### What is Javascript?
- JavaScript is the Programming Language for the Web
- JavaScript can update and change both HTML and CSS
- JavaScript can calculate, manipulate and validate data

# Learning by doing
## 1. Create a new project in your gitlab
Name it whatever you want lol

## 2. Open figma to see the web design
### https://www.figma.com/file/3MmRzLAT7qbV6ZMPjIrJH3/sbf?node-id=0%3A1

## 3. Implement
have fun :)

## 4. Push to your repo
``` 
git add .
git commit -m "insert your message here"
git push origin master
```

## 5. Deploy to netlify
### https://app.netlify.com
